#!/bin/bash


replaces=''
conflicts=''
pkgversion=1.0
pkgname=drainware-v8
pkggroup=drainware
pkgarch=amd64
maintainer=jose.palanco@drainware.com
requires=''
command="cp *.so /opt/drainware/lib"



if [ $# -eq 1  ]
then
sudo checkinstall -y --replaces=$replaces --conflicts=$conflicts --nodoc --pkgversion=$pkgversion \
--pkgrelease=$1 --type=debian --pkgname=$pkgname --pkggroup=$pkggroup --pkgarch=$pkgarch \
--maintainer=$maintainer --requires=$requires --install=no  $command
mv *.deb ..


else
echo "usage: $0 [RELEASE]"
fi


